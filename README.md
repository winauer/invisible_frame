# Invisible Frame

Datapack for Minecraft 1.16+ that makes invisible item frames available in survival. Right click an empty item frame with a glass pane named "Invisible Pane" to make the item frame invisible.